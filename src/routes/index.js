const express = require('express');
const request = require('request');
const router = express.Router();
const MarkdownIt = require('markdown-it');
const md = new MarkdownIt();
const FetchRelease = require('fetchrelease').FetchRelease;
const installerRelease = new FetchRelease({user: "ubports", repo: "ubports-installer", cache_time: 3600}); // One hour cache
const SystemImageClient = require('system-image-node-module').Client;
const systemImageClient = new SystemImageClient({cache_time: 180, path: "./"})

const time = () => Math.floor(new Date() / 1000);
const BASE_URL = "https://api.ubports.com/v1/";

var cache = {}
var testRun = 0;

const getCached = (url) => {
  return new Promise(function(resolve, reject) {
    console.log("cached request", url);
    var now=time();

    if (!cache[url])
      cache[url] = {expire:0};

    // Cache baby cache!!! :D :D
    if (cache[url].expire > now) {
      console.log("use cache")
      resolve(cache[url].data);
      return;
    }

    console.log("get new");
    request({
      method: "get",
      url: url,
      json: true,
      headers: {
        'User-Agent': 'client request: server devices.ubuntu-touch.io'
      }
    }, (err, res, body) => {
      var data = {err: err, res: res, body: body};
      var saveCache = true;

      /* For testing
      if (testRun == 1) {
        console.log("testing err")
        err = true;
      }
      if (testRun == 2) {
        console.log("testing testing 500")
        data.res.statusCode = 500;
      }
      if (testRun == 3) {
        console.log("testing 404")
        data.res.statusCode = 404;
      }
      if (testRun == 4) {
        console.log("testing no data")
        err =true;
        delete cache[url].data;
      }

      testRun++;
      */

      if (err || !res) {
        if (cache[url].data) {
          console.log('Something is wrong with the server, serving cached content');
          data = cache[url].data;
          saveCache = false;
        } else {
          console.log('Something is wrong with the server, but we don\'t have cached contect! rejecting!');
          return reject();
        }
      }

      // Device not found, send reject!
      if(data.res.statusCode === 404) {
        console.log("Server is returning 404!");
        return reject();
      }

      // If we hit an error, try using cache!
      if (data.res.statusCode != 200) {
        console.log('Server is returning', data.res.statusCode, 'serving cached content');
        return resolve(cache[url].data);
      }

      resolve({err: err, res: res, body: body});

      // 3 minutes cache!
      if (saveCache) {
        console.log("saving cache");
        cache[url].expire = time()+10 //time()+180;
        cache[url].data = {err: err, res: res, body: body};
      }
    });
  });
}

// Internal
const getDevices = () => {
  return getCached(BASE_URL+"devices").then(data => {
    return data.body;
  });
}

const getCommunityDevices = () => {
  return getCached(BASE_URL+"devices/community").then(data => {
    return data.body;
  });
}

const getDevice = (device) => {
  return getCached(BASE_URL+"devices/"+device).then(data => {
    if (data.body.about)
      data.body.about = md.render(data.body.about);

    return data.body;
  });
}

const getCommunityDevice = (device) => {
  return getCached(BASE_URL+"devices/community/"+device).then(data => {
    if (data.body.about)
      data.body.about = md.render(data.body.about);

    return data.body;
  });
}

const getInstaller = (installerPackage) => {
  return new Promise((resolve, reject) => {
    switch (installerPackage) {
      case "linux":
      case "snap":
        resolve("https://snapcraft.io/ubports-installer");
        return;
      case "ubuntu":
      case "debian":
      case "mint":
        installerRelease.getAssetUrl("latest", "deb").then((r) => {resolve(r);});
        return;
      case "win":
      case "microsoft":
      case "windows":
        return installerRelease.getAssetUrl("latest", "exe").then((r) => {resolve(r);});
      case "apple":
      case "mac":
      case "macos":
        return installerRelease.getAssetUrl("latest", "dmg").then((r) => {resolve(r);});
      default:
        return installerRelease.getAssetUrl("latest", installerPackage).then((r) => {resolve(r);});
    }
  });
}

function notFound(res) {
  res.status(404);
  res.render('error', {
    message: "Device not found",
    error: {status: 404}
  });
}

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

router.get('/apps', function(req, res, next) {
  res.redirect("https://open-store.io");
});

router.get('/features', function(req, res, next) {
  res.redirect("https://ubuntu-touch.io/features"); // TODO confirm URL
});

router.get('/install', function(req, res, next) {
  res.redirect("https://ubuntu-touch.io/install"); // TODO confirm URL
});

router.get('/convergence', function(req, res, next) {
  res.redirect("https://ubuntu-touch.io/features"); // TODO confirm URL
});

router.get('/design', function(req, res, next) {
  res.redirect("https://ubuntu-touch.io/design"); // TODO confirm URL
});

router.get('/privacy', function(req, res, next) {
  res.redirect("https://ubuntu-touch.io/features"); // TODO confirm URL
});

router.get('/devices', function(req, res, next) {
  res.redirect('/');
});

router.get('/api/devices', function(req, res, next) {
  getDevices().then(r => res.send(r));
});

router.get('/api/devices/community', function(req, res, next) {
  getCommunityDevices().then(r => res.send(r));
});

router.get('/api/device/:device', function(req, res, next) {
  getDevice(req.params.device).then(r => res.send(r)).catch(() => res.send(404));
});

router.get('/api/devices/community/:device', function(req, res, next) {
  getCommunityDevice(req.params.device).then(r => res.send(r)).catch(() => res.send(404));
});

router.get('/device/:device', function(req, res, next) {
  Promise.all([
    getDevice(req.params.device),
    systemImageClient.getDeviceChannels(req.params.device).then(channels => {
      var releases = [];
      channels.forEach((rawChannel) => {
        releases.push(
          systemImageClient.getReleaseDate(req.params.device, rawChannel).then(releaseDate => {
            return {
              "channel": rawChannel.replace("ubports-touch/", ""),
              "date": releaseDate.substring(4, 10) + " " + releaseDate.substring(24, 28)
            };
          })
        );
      });
      return releases;
    }),
    systemImageClient.getLatestVersion(req.params.device, "ubports-touch/16.04/stable").then((latest) => {
      var urls = systemImageClient.getFilesUrlsArray(latest)
      urls.push.apply(urls, systemImageClient.getGgpUrlsArray());
      var files = systemImageClient.getFilePushArray(urls);
      var commands = systemImageClient.createInstallCommands(latest.files);
      return {
        urls: urls,
        files: files,
        commands: commands
      }
    })
  ]).then((r) => {
    // HACK: Wait for release date resuts, since we can't use await.
    Promise.all(r[1]).then(releaseDates => {
      res.render('device', {
        data: r[0],
        releases: releaseDates,
        install: r[2]
      });
    });
  }).catch((e) => {
    console.log(e);
    notFound(res)
  });
});

router.get('/communitydevice/:device', function(req, res, next) {
  getCommunityDevice(req.params.device).then((r) => {
    console.log(JSON.stringify(r))
    res.render('communitydevice', {
      data: r
    });
  }).catch((e) => {
    console.log(e);
    notFound(res)
  });
});

router.get('/installer/:package', function(req, res, next) {
  // redirect to download url of the requested package
  // or package list if the requested package does not exist
  getInstaller(req.params.package).then((r) => {
    res.redirect(r || "https://github.com/ubports/ubports-installer/releases/latest")
  }).catch((e) => {
    console.log(e);
    res.redirect("https://github.com/ubports/ubports-installer/releases/latest");
  });
});

router.get('/installer', function(req, res, next) {
  res.redirect("https://github.com/ubports/ubports-installer/releases/latest");
});

router.get('/telegram', function(req, res, next) {
  res.redirect("https://telegram.me/ubports");
});

module.exports = router;
