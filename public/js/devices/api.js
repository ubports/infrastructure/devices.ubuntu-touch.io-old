var BASE_URL = "/api/"

var get = {
  devices: function(t) {
    return t.$http.get(BASE_URL + "devices")
  },
  communityDevices: function(t) {
    return t.$http.get(BASE_URL + "devices/community")
  },
}
